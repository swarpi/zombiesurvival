﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class WaveSpawner : NetworkBehaviour
{
    [System.Serializable]
    public class Wave
    {
        public string name;
        public Transform enemy;
        public int count;
        public float rate;
    }

    public enum SpawnState
    {
        SPAWNING, WAITING, COUNTING, FINISHED
    }
    public Wave[] waves;
    public Transform[] spawnPoints;
    private int nextWave = 0;

    private Transform enemyToSpawn;

    public float timeBetweenWaves = 5f;
    [SerializeField]
    private float waveCountdown;
    private SpawnState state = SpawnState.COUNTING;

    private float searchCountdown = 1f;

    void Start()
    {
        waveCountdown = timeBetweenWaves;
    }
    [Server]
    void Update()
    {
        if(state == SpawnState.FINISHED)
        {
            Debug.Log("Finished"); // 
            return;

        }
        if (state == SpawnState.WAITING)
        {
            // check enemies are still alive
            if (!EnemyIsAlive())
            {
                WaveCompleted();
                // Begin a new round
                return;
            }
            else
            {
                return;
            }
        }
        if (waveCountdown <= 0)
        {
            if (state != SpawnState.SPAWNING)
            {
                // Start spawning a wave
                StartCoroutine(SpawnWave(waves[nextWave]));
            }
        }
        else
        {
            waveCountdown -= Time.deltaTime;
        }
    }

    void WaveCompleted()
    {
        Debug.Log("wave completed");
        state = SpawnState.COUNTING;
        waveCountdown = timeBetweenWaves;

        if (nextWave + 1 == waves.Length)
        {
            state = SpawnState.FINISHED;
            nextWave = 0; // start again
            Debug.Log("completed all waves");
        }
        else
        {
            nextWave++;
        }

    }

    bool EnemyIsAlive()
    {
        searchCountdown -= Time.deltaTime;
        if (searchCountdown <= 0f)
        {
            searchCountdown = 1f;
            if (GameObject.FindGameObjectWithTag("Enemy") != null)
            {
                return true;
            }
        }
        return false;
    }

    // wait amount of second before continuing
    [Server]
    IEnumerator SpawnWave(Wave wave)
    {
        
        Debug.Log("Spawning Wave: " + wave.name);
        state = SpawnState.SPAWNING;
        for (int i = 0; i < wave.count; i++)
        {
            enemyToSpawn = wave.enemy;
            if (isServer)
            {
                SpawnEnemy();
            }
            yield return new WaitForSeconds(1f / wave.rate);
        }

        state = SpawnState.WAITING;
        Debug.Log("waiting");
        yield break;
    }
    [ClientRpc]
    void RpcSpawnEnemy()
    {
        Debug.Log(enemyToSpawn.name);
        SpawnEnemy();
    }
    void SpawnEnemy()
    {
        //Transform spawnpoint = spawnPoints[Random.Range(0, spawnPoints.Length)];
        //Instantiate(enemy, spawnpoint.position, spawnpoint.rotation);
        Transform enemy = Instantiate(enemyToSpawn, transform.position, transform.rotation);
        NetworkServer.Spawn(enemy.gameObject);
        Debug.Log("spawning Enemy " + enemyToSpawn.name);
    }
}
