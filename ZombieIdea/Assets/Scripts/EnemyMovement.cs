﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class EnemyMovement : MonoBehaviour
{
    AIDestinationSetter AIdest;
    public List<Transform> Target;
    PlayerMovement[] Players;
    // Start is called before the first frame update
    void Awake()
    {
        // add players to targetarray
        Players = FindObjectsOfType<PlayerMovement>();
        Target = new List<Transform>();
        foreach(PlayerMovement p1 in Players)
        {
            Target.Add(p1.transform);
        }

        AIdest = GetComponent<AIDestinationSetter>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Target.Count != 0)
        {
            AIdest.target = Target[0];
        }
        
    }
}
