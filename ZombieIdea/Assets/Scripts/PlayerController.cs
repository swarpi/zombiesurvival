﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private static PlayerController _instance;
    public static PlayerController Instance { get { return _instance; } }
    Transform player;

    [SerializeField]
    private Slider sliderHealth;
    [SerializeField]
    private Slider sliderOverload;
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public void registerPlayer(Transform player)
    {
        this.player = player;
        PlayerStats playerStats = player.gameObject.GetComponent<PlayerStats>();
        sliderHealth.maxValue = playerStats.MaxHP;
        sliderHealth.value = playerStats.CurrentHp;

        Shooting shooting = player.gameObject.GetComponent<Shooting>();
        sliderOverload.maxValue = shooting.overLoadCapacity;
        sliderOverload.value = shooting.overLoad;
    }
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (player)
        {
            sliderHealth.value = player.gameObject.GetComponent<PlayerStats>().CurrentHp;
            sliderOverload.value = player.gameObject.GetComponent<Shooting>().overLoad;
        }
    }
}
