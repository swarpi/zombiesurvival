﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class PlayerMovement : NetworkBehaviour
{
    public float moveSpeed;
    public new Rigidbody2D rigidbody;
    public Vector3 change;

    public bool changeRot = true;
    Vector3 animationVector;

    private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        rigidbody = GetComponent<Rigidbody2D>();
        PlayerController.Instance.registerPlayer(this.transform);
    }

    // Update is called once per frame
    [Client]
    void Update()
    {
        if (!hasAuthority)
        {
            return;
        }
        change = Vector3.zero;
        change.x = Input.GetAxisRaw("Horizontal");
        change.y = Input.GetAxisRaw("Vertical");
        if (Input.GetKeyDown("a") || Input.GetKeyDown("d"))
        {
            animationVector = change;
        }
        else if (Input.GetKeyDown("w") || Input.GetKeyDown("s"))
        {
            animationVector = change;
        }
        if(Input.GetKeyUp("a") || Input.GetKeyUp("d") || Input.GetKeyUp("w") || Input.GetKeyUp("s"))
        {
            animationVector = change;
        }
        
        if (change != Vector3.zero)
        {
            //rigidbody.MovePosition(transform.position + change.normalized * moveSpeed * Time.fixedDeltaTime);
            CmdMove(change); // Im a client calling this function on the server
            // as a client you cant call functions on other clients
            // you have to tell the server your message
        }
    }
    [Command]
    private void CmdMove(Vector3 change)
    {
        Debug.Log("moving player");
        // validate logic for example
        RpcMoveCharacter(change); // the server decides then if it wants to tell the clients to do something
        //moveCharacter();
    }
    [ClientRpc]
    private void RpcMoveCharacter(Vector3 change)
    {
        //transform.Translate(new Vector3(change.x, change.y, 0));
        //transform.position = transform.position + change.normalized * moveSpeed * Time.fixedDeltaTime;
        rigidbody.MovePosition(transform.position + change.normalized * moveSpeed * Time.fixedDeltaTime);
        if (changeRot)
        {
            animator.SetFloat("moveX", animationVector.x);
            animator.SetFloat("moveY", animationVector.y);
        }

    }
 

    public void rotateHorizontal(bool isRight)
    {
        // animation to change rotation
        if (isRight)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else
        {
            transform.rotation = Quaternion.Euler(0, 0, 180);
        }
     
    }
    public void rotateVertical(bool isUp)
    {
        // animation to change rotation
        if (isUp)
        {
            transform.rotation = Quaternion.Euler(0, 0, 90);
        }
        else
        {
            transform.rotation = Quaternion.Euler(0, 0, -90);
        }

    }
}
