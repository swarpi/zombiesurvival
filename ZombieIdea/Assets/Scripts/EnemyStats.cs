﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyStats : CharacterStats
{
    [SerializeField]
    private Slider slider;

    // Start is called before the first frame update
    void Start()
    {
        slider.maxValue = _maxHp;
        slider.value = _maxHp;
        _currentHp = _maxHp;
    }

    // Update is called once per frame
    void Update()
    {
        slider.value = _currentHp; // most likely need to refactor, might cause perfomance issues
    }

    public override void Die()
    {
        Destroy(gameObject);
    }
}
