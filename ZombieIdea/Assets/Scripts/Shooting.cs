﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class Shooting : NetworkBehaviour
{
    public Transform firePoint;
    public GameObject bulletPrefab;
    public Rigidbody2D rigidbody;
    private Animator animator;
    public float offSetAngle = 90;

    public Vector3 mousePos;

    public float bulletForce = 20f;

    // refactor to weapon script later
    public float overLoadCapacity;
    public float overLoadRate;
    float currentOverLoad;
    public float overLoad
    {
        get { return currentOverLoad; }
        set { currentOverLoad = value; }
    }

    public float fireRate = 0f;
    float fireCooldown = 1f;

    [SerializeField]
    private Slider slider;
    private void Start()
    {
        animator = GetComponent<Animator>();
        rigidbody = GetComponent<Rigidbody2D>();

        currentOverLoad = overLoadCapacity;
    }
    // Update is called once per frame
    [Client]
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            
        }
        fireCooldown -= Time.deltaTime;
        if (Input.GetButton("Fire1")) {
            if(currentOverLoad > 0)
            {
                currentOverLoad -= Time.deltaTime * overLoadRate;
            }
            else
            {
                currentOverLoad = 0;
            }
            if (fireCooldown <= 0f && currentOverLoad > 0)
            {
                Debug.Log("holding 1");
                CmdShooting();
                fireCooldown = 1 / fireRate;
            }
        }
        else
        {
            if (currentOverLoad < overLoadCapacity)
            {
                currentOverLoad += Time.deltaTime * overLoadRate;
            }
                
        }
              
    }
    [Command]
    void CmdShooting()
    {
        RpcShooting();
    }
    [ClientRpc]
    void RpcShooting()
    {
        turnPlayer(); // need delay between shooting and faceMouse
        //Shoot();
        // player turns to mouse position
        // shoots in front of player
    }
    void turnPlayer()
    {
        // get mouse position
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 lookDir = mousePosition - rigidbody.position;
        mousePos = lookDir;
       /* Vector2 direction = new Vector2(
            mousePosition.x - transform.position.x,
            mousePosition.y - transform.position.y); 
            */
        float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg;

        //turn player
        if(angle <= 45 && angle > -45) // right
        {
            animator.SetFloat("moveX", 1);
            animator.SetFloat("moveY", 0);
            StartCoroutine(Shoot(new Vector3(1, 0, 0), Quaternion.Euler(0, 0, -90), Quaternion.Euler(0, 0, angle - offSetAngle)));
            //Shoot(new Vector3(1,0,0), Quaternion.Euler(0, 0, -90));
        }
        if (angle <= 135 && angle > 45) // up
        {
            animator.SetFloat("moveX", 0);
            animator.SetFloat("moveY", 1);
            StartCoroutine(Shoot(new Vector3(0, 1, 0), Quaternion.Euler(0, 0, 0), Quaternion.Euler(0, 0, angle - offSetAngle)));
            
        }
        if ((angle <= 180 && angle > 135) || (angle <= - 135 && angle > - 180)) // left
        {
            animator.SetFloat("moveX", -1);
            animator.SetFloat("moveY", 0);
            StartCoroutine(Shoot(new Vector3(-1, 0, 0), Quaternion.Euler(0, 0, 90), Quaternion.Euler(0, 0, angle - offSetAngle)));
            
        }
        if (angle <=- 45 && angle > -135) // down
        {
            animator.SetFloat("moveX", 0);
            animator.SetFloat("moveY", -1);
            StartCoroutine(Shoot(new Vector3(0, -1, 0), Quaternion.Euler(0, 0, -180), Quaternion.Euler(0, 0, angle - offSetAngle)));
        }
    }
    IEnumerator Shoot(Vector3 position, Quaternion rotation, Quaternion angle)
    {
        // rotation only necessary if shooting should be restricted to left right up down
        // otherwise use angle to shoot in every direction
        GetComponent<PlayerMovement>().changeRot = false;
        GameObject bullet = Instantiate(bulletPrefab, firePoint.position, angle);
        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddForce(mousePos.normalized * bulletForce, ForceMode2D.Impulse);

        yield return new WaitForSeconds(0.5f);
        GetComponent<PlayerMovement>().changeRot = true;
    }


}