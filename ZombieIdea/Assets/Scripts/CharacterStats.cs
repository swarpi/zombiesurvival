﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    [SerializeField] protected float _maxHp;
    [SerializeField] protected float _currentHp;
    [SerializeField] protected float _damage;

    public float MaxHP
    {
        get { return _maxHp; }
        set { _maxHp = value; }
    }
    public float CurrentHp
    {
        get { return _currentHp; }
        set { _currentHp = value; }
    }

    // Start is called before the first frame update
    void Start()
    {
        //currentHp = maxHp;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void takeDamage(float damage)
    {
        _currentHp -= damage;
        if(_currentHp < 0)
        {
            Die();
        }

        // reduce health
    }
    // override me
    public virtual void Die()
    {

    }
}
