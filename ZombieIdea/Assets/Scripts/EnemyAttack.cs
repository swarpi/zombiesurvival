﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    private float attackCooldown = 0f;
    public float attackSpeed = 1f;
    public Animator animator;
    Transform target;
    public float attackRadius = 3.5f;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            target = collision.gameObject.transform;
            Debug.Log("target is " + target.name);
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            target = collision.gameObject.transform;
            Debug.Log("target is " + target.name);
        }
    }

    // Update is called once per frame
    void Update()
    {
        attackCooldown -= Time.deltaTime;
        if (target)
        {
            float distance = Vector3.Distance(target.position, transform.position);
            
            if (distance <= attackRadius)
            {
                StartCoroutine(AttackPlayer());
            }
        }

    }

    IEnumerator AttackPlayer()
    {
        if (attackCooldown <= 0f) // then you can attack
        {
            Debug.Log("Attacking player");
            // attack
            animator.SetBool("Attacking",true);
            target.gameObject.GetComponent<PlayerStats>().takeDamage(1);
            attackCooldown = 1 / attackSpeed;

            yield return new WaitForSeconds(0.5f);

            animator.SetBool("Attacking", false);
        }
    }
}
