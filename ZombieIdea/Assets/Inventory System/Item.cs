﻿using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Item
{
    public enum ItemType
    {
        Pistol,
        Sniper,
        Rifle,
        HealthPotion

    }
    public ItemType itemType;
    public int amount;

    public Sprite GetSprite()
    {
        switch (itemType)
        {
            default:
            case ItemType.Pistol:              return ItemAssets.Instance.pistolSprite;
            case ItemType.Sniper:             return ItemAssets.Instance.sniperSprite;
            case ItemType.Rifle:                return ItemAssets.Instance.rifleSprite;
            case ItemType.HealthPotion: return ItemAssets.Instance.healthPotionSprite;
        }

       
    }
    public bool IsStackable()
    {
        switch (itemType)
        {
            default:
            case ItemType.HealthPotion:
                return true;
            case ItemType.Pistol:
            case ItemType.Rifle:
            case ItemType.Sniper:
                return false;
        }
    }
}
